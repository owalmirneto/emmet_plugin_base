$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'emmet_plugin_base/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'emmet_plugin_base'
  s.version     = EmmetPluginBase::VERSION
  s.authors     = ['Walmir Neto']
  s.email       = ['wfsneto@gmail.com']
  s.homepage    = 'https://bitbucket.org/wfsneto/emmet_plugin_base'
  s.summary     = 'Emmet Plugin Base'
  s.description = 'This is an example of rails plugin'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']

  s.required_ruby_version = '>= 2.2.0'

  s.add_dependency 'rails', '~> 4.2', '>= 4.2.6'

  s.add_development_dependency 'sqlite3', '~> 0'
  s.add_development_dependency 'rspec-rails', '~> 3.4', '>= 3.4.2'
  s.add_development_dependency 'rubocop', '~> 0.36.0'
  s.add_development_dependency 'shoulda-matchers', '~> 3.1', '>= 3.1.1'
  s.add_development_dependency 'factory_girl_rails', '~> 4.6'
  s.add_development_dependency 'pry', '~> 0.10.3'
end
