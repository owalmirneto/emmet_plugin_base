Rails.application.routes.draw do
  root 'home#index'

  mount EmmetPluginBase::Engine => :emmet_plugin_base
end
